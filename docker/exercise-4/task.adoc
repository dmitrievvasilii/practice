== Задание 4. Publish Docker образ в registry

Для выполнения задания необходимо залогиниться в приватный Gitlab registry.
Для этих целей используется `Gitlab Deploy Key`, который имеет вид `name/password` и выдается тренером.

[source, bash]
----
docker logout registry.gitlab.com   <1>
Removing login credentials for registry.gitlab.com
docker login registry.gitlab.com <2>
Username: <type username> <3>
Password: <type password> <4>
WARNING! Your password will be stored unencrypted in $HOME/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
----
<1> logout из registry.gitlab.com
<2> login в registry.gitlab.com
<3> `<type username> ` имя пользователя, выданное тренером.
<4> `<type password> ` пароль, выданный тренером.

После этого можно приступать к выполнению задания.

Используя образ созданный в `Задание 2`, выполните следующее:

. запаблишить образ в частный Gitlab Docker Registry
.. откройте в браузере https://gitlab.com/cloud-native-development1/module-1/practice/container_registry/eyJuYW1lIjoiY2xvdWQtbmF0aXZlLWRldmVsb3BtZW50MS9tb2R1bGUtMS9wcmFjdGljZS9kb2NrZXIvdGFzazIiLCJ0YWdzX3BhdGgiOiIvY2xvdWQtbmF0aXZlLWRldmVsb3BtZW50MS9tb2R1bGUtMS9wcmFjdGljZS9yZWdpc3RyeS9yZXBvc2l0b3J5LzExMTU3MzIvdGFncz9mb3JtYXQ9anNvbiIsImlkIjoxMTE1NzMyfQ==[Gitlab Container Registry^] и убедитесь что ваш образ опубликовался.
+
image::gitlab-registry.png[]
